package backend;

import java.util.ArrayList;

public class Arista {
	
	Nodo nodoA;
	Nodo nodoB;
	int precioDeKm;
	double precio;
	
	public Arista (Nodo nodoA, Nodo nodoB, int precioDeKM) {
		
		this.nodoA = nodoA;
		this.nodoB = nodoB;
		this.precioDeKm = precioDeKM;
		this.precio = Logica.distanciaEntreCoordinadas(nodoA.coordinates, nodoB.coordinates)*precioDeKM;
		
		//Si la distancia entre nodos es mayor a 200, se suma el precio extra
		if(Logica.distanciaEntreCoordinadas(nodoA.coordinates, nodoB.coordinates) > 200) {
			
			this.precio += nodoA.getPrecioExtra();
			
		}
		
		//Si las provincias de los nodos son distintas, se suma el precio extra
		if(!nodoA.getProvincia().equals(nodoB.getProvincia())){
			
			this.precio += nodoA.getPrecioExtra();
			
		}
		
	}
	
	/**
	 * Funcion que devuelve la arista de menor precio de conexión
	 * @param ArrayList <Arista> aristas
	 * @return Arista
	 */
	public static Arista mejorArista(ArrayList <Arista> aristas) {
		
		Arista mejorArista = aristas.get(0);
		
		for (Arista arista : aristas) {
			
			if (arista.getPrecioDeConexion() < mejorArista.getPrecioDeConexion()) {
				
				mejorArista = arista;
				
			}
			
		}
		
		return mejorArista;
		
	}
	
	public Nodo getNodoA () {
		
		return this.nodoA;
		
	}
	
	public Nodo getNodoB () {
		
		return this.nodoB;
		
	}
	
	public int getPrecioDeKm() {
		
		return this.precioDeKm;
		
	}
	
	public double getPrecioDeConexion () {
		
		return this.precio;
		
	}
	

}
