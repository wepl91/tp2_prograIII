package backend;

import java.util.ArrayList;

public class GeneradorCaminoMinimo {
	
	private static ArrayList<Arista> aristas;
	private static ArrayList<Nodo> nodos;
	
	
	public GeneradorCaminoMinimo(Grafo g){
		aristas = g.getAristas();
		nodos = g.getNodos();
	}
	
	/**
	 * Implementacion de algoritmo de Prim
	 * @param localidadesAgregadas
	 * @param caminos
	 * @return
	 */
	public ArrayList <Arista> generarArbolMinimo(){
		
		ArrayList <Nodo> nodosAgregados = new ArrayList<Nodo>();
		//Agarro el primer nodo
		nodosAgregados.add(nodos.get(0));
		ArrayList <Arista> aristasDisponibles = new ArrayList<Arista>();
		Arista mejorArista = null; 
		ArrayList <Arista> arbolGeneradorMinimo = new ArrayList<Arista>();
		
		//Ciclo que recorre todos los nodos del grafo
		while (nodosAgregados.size() < nodos.size()) {
			
			//Agarro las aristas relacionadas a los nodos que tengo
			aristasDisponibles = agregarCaminosDisponibles(nodosAgregados, aristas);
			mejorArista = Arista.mejorArista(aristasDisponibles);
			arbolGeneradorMinimo.add(mejorArista);
			nodosAgregados = agregarLocalidades(nodosAgregados,mejorArista);
		
		}
		
		return arbolGeneradorMinimo;
	}

	public static ArrayList<Nodo> agregarLocalidades(ArrayList <Nodo> localidadesAgregadas, Arista mejorCamino) {
		
		for(int i = 0; i < localidadesAgregadas.size() ; i++){
		
			if(localidadesAgregadas.get(i).equals(mejorCamino.getNodoA())){
				localidadesAgregadas.add(mejorCamino.getNodoB());

				return localidadesAgregadas;
			}
			
			if(localidadesAgregadas.get(i).equals(mejorCamino.getNodoB())){
				localidadesAgregadas.add(mejorCamino.getNodoA());

				return localidadesAgregadas;
			}
			
		}
		
		return localidadesAgregadas;
	}

	private static ArrayList <Arista> agregarCaminosDisponibles(ArrayList <Nodo> localidadesAgregadas, ArrayList <Arista> caminos) {
		
		ArrayList <Arista> caminosDisponibles = new ArrayList<Arista>();
		
		for (int i = 0; i < localidadesAgregadas.size(); i++) {
		
			for(int e = 0; e < caminos.size(); e++){
			
				if((caminos.get(e).getNodoA().equals(localidadesAgregadas.get(i)) && !localidadesAgregadas.contains(caminos.get(e).getNodoB())) || (caminos.get(e).getNodoB().equals(localidadesAgregadas.get(i)) && !localidadesAgregadas.contains(caminos.get(e).getNodoA()))){
					caminosDisponibles.add(caminos.get(e));
				}
			}
		}
		return caminosDisponibles;
	}
	
	/**
	 * Funcion que calcula el precio final de la conexion
	 * @param caminoMinimo
	 * @return
	 */
	public static double calcularPrecioFinal(ArrayList<Arista>caminoMinimo ) {
		
		double precio = 0;
		
		for (Arista arista : caminoMinimo) {
			precio += arista.getPrecioDeConexion();
		}
		return precio;
	}
	
}
