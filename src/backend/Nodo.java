package backend;

import org.openstreetmap.gui.jmapviewer.Coordinate;

public class Nodo {
	
	Coordinate coordinates;
	String prov;
	String loc;
	long habitants;
	int precioExtra;
	
	public Nodo (Coordinate coordinates, String prov, String loc, long habitants, int precioExtra) {
		
		this.coordinates = coordinates;
		this.habitants = habitants;
		this.precioExtra = precioExtra;
		this.prov = prov;
		
	}
	
	public Coordinate getCoordenadas () {
		
		return this.coordinates;
		
	}
	
	public int getPrecioExtra() {
		
		return this.precioExtra;
		
	}
	
	public String getProvincia() {
		
		return this.prov;
		
	}
		
}
