package backend;

import org.openstreetmap.gui.jmapviewer.Coordinate;

public class Logica {
		
	/**
	 * Funcion que devuelve la distancia entre 2 puntos dependiendo de las coordenadas de los mismos
	 * @param coordenada de punto A
	 * @param coordenada de punto B
	 * @return
	 */
	public static double distanciaEntreCoordinadas (Coordinate coord1, Coordinate coord2) {
		
		double latDist = Math.toRadians(coord1.getLat() - coord2.getLat());
		double longDist = Math.toRadians(coord1.getLon() - coord2.getLon());
		
		double a = (Math.sin(latDist/2) * Math.sin(latDist/2)) + (Math.cos(Math.toRadians(coord1.getLat())) * Math.cos(Math.toRadians(coord2.getLat()))) * Math.sin(longDist/2) * Math.sin(longDist/2);
		double c = 2 * Math.atan2(Math.sqrt(a),Math.sqrt(1-a));
		
		return (int) (Math.round(6371 * c)); //6371 es el diametro de la tierra en km
		
	}
	
	/**
	 * Validacion para inputs de provincia, localidad y nro de habitantes
	 * @param String prov
	 * @param String loc
	 * @param String hab
	 * @return String advise
	 */
	public static boolean emptyInput (String prov, String loc, String hab) {
		
		if (prov.equals("") || loc.equals("") || hab.equals("")) {
			
			return true;
		
		}
		
		return false;
	}
	
	/**
	 * Funcion de devuelve el precio extra total
	 * @param precioExtra de conexion mayor de 200 km
	 * @param precioExtra de conexion entre 2 provincias distintas
	 * @return
	 */
	public static int getExtra (String precioExtra1, String precioExtra2) {
		
		int ret = 0;
		
		if (precioExtra1 != null) {
		
			ret = ret + Integer.parseInt(precioExtra1);
		
		}
		
		if (precioExtra2 != null) {
			
			ret = ret + Integer.parseInt(precioExtra2);
			
		}
		
		return ret;
		
	}

}
