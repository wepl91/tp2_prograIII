package backend;

import java.util.ArrayList;

public class Grafo {
	

 	ArrayList <Nodo> nodos;
 	ArrayList <Arista> aristas;
 	
 
 	public Grafo() {
 		
 		nodos = new ArrayList <Nodo>();
 		aristas = new ArrayList <Arista>();
 	
 	}
 
 	public void agregarNodo (Nodo nodoNuevo, int precioKm) {
 		
 		nodos.add(nodoNuevo);
 		
 		for (Nodo nodo : nodos) {
 			
 			Arista aristaNueva = new Arista(nodo, nodoNuevo, precioKm);
 			aristas.add(aristaNueva);
 			
 		}
 		
 	}
 	public ArrayList <Nodo> getNodos() {
 		
 		return nodos;
 	
 	}
 
 	public ArrayList <Arista> getAristas() {
 	
 		return aristas;
 	
 	}

}
