package frontend;

import java.awt.EventQueue;
import java.awt.Point;
import javax.swing.JFrame;
import javax.swing.UIManager;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

import backend.Grafo;
import backend.Logica;
import backend.GeneradorCaminoMinimo;
import backend.Nodo;
import backend.Arista;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;

public class Mainform {

	private JFrame frm;
	private JTextField txtPrecio;
	private JTextField txtProv;
	private JTextField txtLoc;
	private JTextField txtHab;
	private JTextField txtExtraDosProv;
	private JTextField txtExtraConexionExtensa;

	public static void main(String[] args) {
		
	    try { 
	        UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel"); 
	    } catch(Exception e) {  }
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Mainform window = new Mainform();
					window.frm.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Mainform() {
		initialize();
	}

	private void initialize() {

		Grafo grafo = new Grafo();
		frm = new JFrame();
		frm.setTitle("A que no sabes de donde te estoy llamando?");
		frm.setBounds(250, 50, 800, 747);
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frm.getContentPane().setLayout(null);
		
		final JMapViewer map = new JMapViewer();		
		map.setDisplayPositionByLatLon(-34.6036844, -58.381559100000004, 5);
		map.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		map.setBounds(12,12,775,400);
		frm.getContentPane().add(map);
		
		//Botón para cerrar el programa//
		JButton btnClose = new JButton("Close");
		btnClose.setToolTipText("Close window");
		btnClose.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) { System.exit(0); }
		});
		
		btnClose.setBounds(6, 684, 117, 25);
		frm.getContentPane().add(btnClose);
		
		//Instrucción//
		JLabel lblConsigna = new JLabel("Ingrese primero los datos en los cuadros de texto y \nluego los puntos que desea conectar en el mapa.");
		lblConsigna.setBounds(12, 412, 697, 25);
		frm.getContentPane().add(lblConsigna);
		
		//Label y TextField de ingreso de Precio de KM//
		JLabel lblPrecioDeKm = new JLabel("Precio de km:");
		lblPrecioDeKm.setBounds(438, 444, 85, 25);
		frm.getContentPane().add(lblPrecioDeKm);
		
		txtPrecio = new JTextField();
		txtPrecio.setBounds(541, 443, 122, 27);
		frm.getContentPane().add(txtPrecio);
		txtPrecio.setColumns(10);
		
		//Label y TextField de ingreso de Provincia//
		JLabel lblProvincia = new JLabel("Provincia:");
		lblProvincia.setBounds(462, 481, 60, 15);
		frm.getContentPane().add(lblProvincia);
		
		txtProv = new JTextField();
		txtProv.setBounds(541, 475, 122, 27);
		frm.getContentPane().add(txtProv);
		txtProv.setColumns(10);
		
		//Label y TextField de ingreso de Localidad//
		JLabel lblNewLabel = new JLabel("Localidad: ");
		lblNewLabel.setBounds(462, 516, 67, 15);
		frm.getContentPane().add(lblNewLabel);

		txtLoc = new JTextField();
		txtLoc.setBounds(541, 510, 122, 27);
		frm.getContentPane().add(txtLoc);
		txtLoc.setColumns(10);
		
		//Label y TextField de ingreso de Cantidad de habitantes//
		JLabel lblCantidadDeHabitantes = new JLabel("Cantidad de habitantes: ");
		lblCantidadDeHabitantes.setBounds(373, 548, 156, 18);
		frm.getContentPane().add(lblCantidadDeHabitantes);
		
		txtHab = new JTextField();
		txtHab.setBounds(541, 544, 122, 27);
		frm.getContentPane().add(txtHab);
		txtHab.setColumns(10);
		
		//Botón para calcular el recorrido minimo//
		JButton btnCalculate = new JButton("Calculate");
		btnCalculate.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
			
				GeneradorCaminoMinimo generadorCominoMinimo = new GeneradorCaminoMinimo(grafo);
				ArrayList  <Arista> caminoMinimo = generadorCominoMinimo.generarArbolMinimo();
				ArrayList < Coordinate > coordenadas = new ArrayList<Coordinate>();
				
				//Agrego las coordenadas de la primer arista, para luego agregar las coordinadas del 2 nodo del resto de las aristas
				coordenadas.add(caminoMinimo.get(0).getNodoA().getCoordenadas());
				coordenadas.add(caminoMinimo.get(0).getNodoB().getCoordenadas());

				for (Arista arista : caminoMinimo) {
					ArrayList<Coordinate> camino = new ArrayList<Coordinate>(Arrays.asList(arista.getNodoA().getCoordenadas(), arista.getNodoB().getCoordenadas(), arista.getNodoB().getCoordenadas()));
					map.addMapPolygon(new MapPolygonImpl(camino));
				}
				
				JOptionPane.showMessageDialog(null,"El precio final de la conexion es: " + GeneradorCaminoMinimo.calcularPrecioFinal(caminoMinimo));
		
			}
		});
		
		btnCalculate.setToolTipText("Calcular el recorrido minimo entre los puntos que se quiere conectar ");
		btnCalculate.setBounds(541, 684, 122, 25);
		frm.getContentPane().add(btnCalculate);
		
		JLabel lblCargoExtraPor = new JLabel("Cargo extra por conexion entre dos provincias:");
		lblCargoExtraPor.setBounds(234, 589, 295, 15);
		frm.getContentPane().add(lblCargoExtraPor);
		
		txtExtraDosProv = new JTextField();
		txtExtraDosProv.setBounds(541, 583, 122, 27);
		frm.getContentPane().add(txtExtraDosProv);
		txtExtraDosProv.setColumns(10);
		
		JLabel lblCargoExtraPor_1 = new JLabel("Cargo extra por conexion mayor a 200 km:");
		lblCargoExtraPor_1.setBounds(263, 622, 266, 15);
		frm.getContentPane().add(lblCargoExtraPor_1);
		
		txtExtraConexionExtensa = new JTextField();
		txtExtraConexionExtensa.setBounds(541, 616, 122, 27);
		frm.getContentPane().add(txtExtraConexionExtensa);
		txtExtraConexionExtensa.setColumns(10);

		//Evento onClick del mapa, en este evento se van creando los nodos
		map.addMouseListener(new MouseAdapter() {
			
			//Ingresar una marca de coordenada por mouse en el mapa//
			public void mouseClicked(MouseEvent e) {
				
				//Dejo deshabilitada la edicion de los textbox ya que van a ser costos fijos
				txtPrecio.setEditable(false);
				txtPrecio.setEnabled(false);
				txtExtraConexionExtensa.setEditable(false);
				txtExtraConexionExtensa.setEnabled(false);
				txtExtraDosProv.setEditable(false);
				txtExtraDosProv.setEnabled(false);
				
				String prov = txtProv.getText();
				String loc = txtLoc.getText();
				String hab = txtHab.getText();
				String extraConexionExtensa = txtExtraConexionExtensa.getText();
				String extraConexionEntreProvincias = txtExtraDosProv.getText();
				
				if  (!Logica.emptyInput(prov, loc, hab)) {
				
					Point point = e.getPoint();
					Coordinate coord = map.getPosition(point);
					MapMarker marker = new MapMarkerDot(coord);
					marker.getStyle().setBackColor(Color.magenta);
					marker.getLayer();
					map.addMapMarker(marker);
					int precioKM = Integer.parseInt(txtPrecio.getText());
					
					grafo.agregarNodo(new Nodo(coord, prov, loc, Long.parseLong(hab), Logica.getExtra(extraConexionExtensa, extraConexionEntreProvincias)), precioKM);
					
					txtLoc.setText("");
					txtProv.setText("");
					txtHab.setText("");
					
				} else {
					
					JOptionPane.showMessageDialog(null,"Falta algun dato importante de la localidad que desea agregar, por favor verifique todos los datos.");
					
				}
				
			}
		});

	}
}