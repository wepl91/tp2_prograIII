package asserts;
import static org.junit.Assert.*;

import java.awt.Point;
import java.util.ArrayList;

import org.junit.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;

import backend.Arista;
import backend.GeneradorCaminoMinimo;
import backend.Grafo;
import backend.Nodo;

public class CaminoMinimoTest {
	
	public Grafo generarGrafo() {
		
		Grafo grafo = new Grafo();
		
		Point pointNodoA = new Point(4, 9);
		Nodo nodoA = new Nodo(new Coordinate(pointNodoA.getX(), pointNodoA.getY()), "Buenos Aires", "Mar del Plata", 500, 50);
		
		grafo.agregarNodo(nodoA, 100);
		
		Point pointNodoB = new Point(10, 11);
		Nodo nodoB = new Nodo(new Coordinate(pointNodoB.getX(), pointNodoB.getY()), "Cordoba", "Calamuchita", 750, 50);
		
		grafo.agregarNodo(nodoB, 100);
		
		Point pointNodoA2 = new Point(10,11);
		Nodo nodoA2 = new Nodo(new Coordinate(pointNodoA2.getX(), pointNodoA2.getY()), "Cordoba", "Calamuchita", 1500, 50);
		
		grafo.agregarNodo(nodoA2, 100);
		
		Point pointNodoB2 = new Point(8,0);
		Nodo nodoB2 = new Nodo(new Coordinate(pointNodoB2.getX(), pointNodoB2.getY()), "Mendoza", "San Rafael", 200, 50);
		
		grafo.agregarNodo(nodoB2, 100);
		
		Point pointNodoA3 = new Point(8,0);
		Nodo nodoA3 = new Nodo(new Coordinate(pointNodoA3.getX(), pointNodoA3.getY()), "Mendoza", "San Rafael", 600, 50);
		
		grafo.agregarNodo(nodoA3, 100);
		
		Point pointNodoB3 = new Point(17,20);
		Nodo nodoB3 = new Nodo(new Coordinate(pointNodoB3.getX(), pointNodoB3.getY()), "La Pampa", "Santa Rosa", 850, 50);
		
		grafo.agregarNodo(nodoB3, 100);
		
		Point pointNodo = new Point(45,75);
		Nodo nodo = new Nodo(new Coordinate(pointNodo.getX(), pointNodo.getY()), "Rio Negro", "Rio Negro", 850, 50);
		
		grafo.agregarNodo(nodo, 100);

		return grafo;
	
	}
	
	@Test
	public void testGrafo() {
		Grafo grafo = generarGrafo();
		
		GeneradorCaminoMinimo generadorCominoMinimo = new GeneradorCaminoMinimo(grafo);
		ArrayList  <Arista> caminoMinimo = generadorCominoMinimo.generarArbolMinimo();
		
		assertTrue(GeneradorCaminoMinimo.calcularPrecioFinal(caminoMinimo) == 899400);
		assertFalse(GeneradorCaminoMinimo.calcularPrecioFinal(caminoMinimo) > 899400);
		assertFalse(GeneradorCaminoMinimo.calcularPrecioFinal(caminoMinimo) < 899400);
		
	}

}
