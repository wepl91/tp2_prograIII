package asserts;
import static org.junit.Assert.*;
import backend.Logica;
import java.awt.Point;

import org.junit.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;

public class LogicaTest {
	
	Point point1 = new Point(5, 4);
	Coordinate coord1 = new Coordinate(point1.getX(), point1.getY());
	Point point2 = new Point(5, 4);
	Coordinate coord2 = new Coordinate(point2.getX(), point2.getY());
	Point point3 = new Point(15, 34);
	Coordinate coord3 = new Coordinate(point3.getX(), point3.getY());
	
	@Test
	public void testDistancias() {
		assertTrue(Logica.distanciaEntreCoordinadas(coord1, coord2) == 0);
		assertFalse(Logica.distanciaEntreCoordinadas(coord1, coord2) != 0);
		assertTrue(Logica.distanciaEntreCoordinadas(coord1, coord3) != 0);
		assertFalse(Logica.distanciaEntreCoordinadas(coord3, coord2) == 0);
	}
	
	@Test
	public void testExtra() {
		
		assertTrue(Logica.getExtra(null, "0") == 0);
		assertTrue(Logica.getExtra("0", null) == 0);
		assertTrue(Logica.getExtra(null, null) == 0);
		assertTrue(Logica.getExtra("0", "0") == 0);
		assertTrue(Logica.getExtra(null, "1") > 0);
		assertTrue(Logica.getExtra("2", null) > 0);
		assertTrue(Logica.getExtra("4", "2") > 0);
		
	}
	
	@Test
	public void testEmptyInput() {
		
		assertTrue(Logica.emptyInput("", "", ""));
		assertTrue(Logica.emptyInput("Buenos Aires", "", ""));
		assertFalse(Logica.emptyInput("Buenos Aires", "Mart del Plata", "1250"));
		
	}
}
