package asserts;
import static org.junit.Assert.*;

import java.awt.Point;
import java.util.ArrayList;

import org.junit.Test;
import org.openstreetmap.gui.jmapviewer.Coordinate;

import backend.Arista;
import backend.Nodo;

public class AristaTest {

	
	public ArrayList<Arista> generarAristas(){
		
		ArrayList<Arista> aristas = new ArrayList<>();
		Point pointNodoA = new Point(4, 9);
		Nodo nodoA = new Nodo(new Coordinate(pointNodoA.getX(), pointNodoA.getY()), "Buenos Aires", "Mar del Plata", 500, 50);
		
		Point pointNodoB = new Point(10, 11);
		Nodo nodoB = new Nodo(new Coordinate(pointNodoB.getX(), pointNodoB.getY()), "Cordoba", "Calamuchita", 750, 50);
		
		Arista arista1 = new Arista(nodoA, nodoB, 100);
		aristas.add(arista1);

		Point pointNodoA2 = new Point(10,11);
		Nodo nodoA2 = new Nodo(new Coordinate(pointNodoA2.getX(), pointNodoA2.getY()), "Cordoba", "Calamuchita", 1500, 50);
		
		Point pointNodoB2 = new Point(8,0);
		Nodo nodoB2 = new Nodo(new Coordinate(pointNodoB2.getX(), pointNodoB2.getY()), "Mendoza", "San Rafael", 200, 50);
		
		Arista arista2 = new Arista(nodoA2, nodoB2, 100);
		aristas.add(arista2);
		
		Point pointNodoA3 = new Point(8,0);
		Nodo nodoA3 = new Nodo(new Coordinate(pointNodoA3.getX(), pointNodoA3.getY()), "Mendoza", "San Rafael", 600, 50);

		Point pointNodoB3 = new Point(17,20);
		Nodo nodoB3 = new Nodo(new Coordinate(pointNodoB3.getX(), pointNodoB3.getY()), "La Pampa", "Santa Rosa", 850, 50);
		
		Arista arista3 = new Arista(nodoA3, nodoB3, 100);
		aristas.add(arista3);
		return aristas;
		
	}
	
	@Test
	public void testAristas() {
		ArrayList<Arista> aristas = generarAristas();
		assertTrue(Arista.mejorArista(aristas).equals(aristas.get(0)));
		assertFalse(Arista.mejorArista(aristas).equals(aristas.get(1)));
		assertFalse(Arista.mejorArista(aristas).equals(aristas.get(2)));
	}
}
